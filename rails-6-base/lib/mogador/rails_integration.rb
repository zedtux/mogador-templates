# frozen_string_literal: true

#
# Rails integration for Mogador.
#
# This file takes care of binding this Rails template with an application.
#

puts '[rails-base] Rails 6 Mogador integration ...'
puts "[rails-base] Rails.env: #{Rails.env.inspect}"
puts "[rails-base] Rails.root: #{Rails.root.inspect}"

# module Rails
#   #
#   # Overrides a method which creates temporaries folders.
#   #
#   # This override creates them in the app's folder, where the user running the
#   # app is expected to have the permissions to create them.
#   #
#   class Server < ::Rack::Server
#     private

#     #
#     # Overrides the tmp subfolder creation methods in order to redirect them to
#     # the user's app folder avoiding permission issues.
#     #
#     def create_tmp_directories
#       puts '[rails-base] Rails::Server#create_tmp_directories override !'

#       %w[cache pids sockets].each do |dir_to_make|
#         dir = File.join(Mogador.app_root, 'tmp', dir_to_make)
#         puts "[rails-base] Creating dir #{dir} ..."
#         FileUtils.mkdir_p(dir)
#       end
#     end
#   end
# end

#
# Prepend all Rails paths with the dev's app folders
#
# See https://github.com/rails/rails/blob/6-0-stable/railties/lib/rails/engine/configuration.rb
#
%w[
  app
  app/assets
  app/controllers
  app/channels
  app/helpers
  app/models
  app/mailers
  app/views

  lib
  lib/assets
  lib/tasks

  config
  config/environments
  config/initializers
  config/locales
  config/routes.rb

  db
  db/migrate
  db/seeds.rb

  vendor
  vendor/assets
].each do |path|
  puts "[rails-base] Configuring #{path} ..."
  Rails.application.paths[path].unshift(Mogador.app_root.join(path))
end
