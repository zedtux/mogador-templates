# Rails 6 Mogdaor template

This Rails 6 template has been generated using `rails new rails-6-base`.

Mogador integration is loaded from the `config/environment.rb` file and the integration is located at `lib/mogador/rails_integration.rb`.

## Usage

Have a look at [the Mogador gem repository](https://gitlab.com/zedtux/mogador) in order to see how to install and use mogador.
