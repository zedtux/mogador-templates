# Load the Rails application.
require_relative 'application'

puts '[base-rails] Loading Rails 6 Mogador integration from config/environment ...'
require File.expand_path('../lib/mogador/rails_integration', __dir__)

# Initialize the Rails application.
Rails.application.initialize!
