# Mogador templates

This repository stores the installable [Mogador](https://gitlab.com/zedtux/mogador) templates.

Those templates are installable, like you install Ruby versions, on your machine, and Mogador allows you to _bind_ your app to the template.
_See [the Mogador testapps](https://gitlab.com/zedtux/mogador-testapps) repo for examples._

Mogador allows you to decouple your app's code from the framework's structure so that you work on your app only (no more framework files you have no idea what they are doing and why they're here) but even better, you can switch from one to another easing the migration.

See [the Mogador gem repository](https://gitlab.com/zedtux/mogador) for more details.
